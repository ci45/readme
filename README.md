# POC gitlab-runner _on premise_ pour gitlab.com

![archi](./gitlab-ci.png?raw=true)

## Raspberry pi
```
pi@raspberrypi:/home/gitlab-runner/maven $ free -h
              total        used        free      shared  buff/cache   available
Mem:           926M        674M         53M         10M        199M        188M
```

## gitlab-runner

Il n'y a pas besoin d'ouvrir de port sur le router / firewall. C'est le gitlab-runner qui scrute [gitlab.com](gitlab.com).
```
pi@raspberrypi:/home/gitlab-runner/maven $ ps -aux | grep gitlab-runner
root      2818  0.0  0.9 835944  9180 ?        Ssl  Jul08   2:26 /usr/bin/gitlab-runner run --working-directory /home/gitlab-runner --config /etc/gitlab-runner/config.toml --service gitlab-runner --user gitlab-runner
```

Le process du gitlab-runner (ici PID 2818) ouvre une socket vers \[2606:4700:90:0:f22e:fbec:5bed:a9b9\]:443 :
```
pi@raspberrypi:/home/gitlab-runner/maven $ sudo lsof -Pan -p 2818 -i
COMMAND    PID USER   FD   TYPE DEVICE SIZE/OFF NODE NAME
gitlab-ru 2818 root    5u  IPv6 106530      0t0  TCP [2a01:e0a:8e3:4af0:30a2:20c9:70e0:282e]:46004->[2606:4700:90:0:f22e:fbec:5bed:a9b9]:443 (ESTABLISHED)
```

C'est bien gitlab.com :
```
pi@raspberrypi:/home/gitlab-runner/maven $ nslookup
> set q=AAAA
> gitlab.com
Server:         192.168.0.254
Address:        192.168.0.254#53

Non-authoritative answer:
gitlab.com      has AAAA address 2606:4700:90:0:f22e:fbec:5bed:a9b9
```

Le runner requête gitlab.com avec l'API GitLab CI. Il récupère une liste de build à effectuer et pendant chaque build il upload les logs, le status...

La configuration du runner est définie dans `/etc/gitlab-runner/config.toml`.

La fréquence de scrutation (pour récupérer les builds) peut être ajustée avec le paramètre [check_interval](https://docs.gitlab.com/runner/configuration/advanced-configuration.html#how-check_interval-works)

Pour ce POC, j'ai créé un exécuteur `bash`. Cf [https://docs.gitlab.com/runner/executors/index.html](https://docs.gitlab.com/runner/executors/index.html)